﻿using System;
namespace BacktrackAlgorithm
{
    public static class View

    {
        public const string WELCOME = "Create a node graph by adding node pairs and \nthen find the shortest or longest path between nodes.";
        public const string HELP = "Enter command or type 'help'";
        public const string INSTRUCT_1 = "add N1 N2 \t\t\t\t(Add bi-directional nodes)";
        public const string INSTRUCT_2 = "break N1 N2 [-p preserve orphans] \t(Break node connection)";
        public const string INSTRUCT_3 = "find N1 N2 [-s shortest] [-l longest] \t(Find path between nodes)";
        public const string INSTRUCT_4 = "print N1 \t\t\t\t(Display connected nodes)";
        public const string INSTRUCT_5 = "clear \t\t\t\t\t(Delete all nodes to start over)";

        public static void PrintBlank()
        {
            Console.WriteLine(string.Empty);
        }
        public static void PrintWelcome()
        {
            Console.WriteLine(View.WELCOME);
        }
        public static void PrintDefaultPrompt()
        {
            Console.WriteLine(View.HELP);
        }
        public static void PrintCommandResult(string consoleMessage)
        {
            Console.WriteLine(consoleMessage);
        }
        public static void PrintHelpMessage()
        {
            Console.WriteLine(View.INSTRUCT_1);
            Console.WriteLine(View.INSTRUCT_2);
            Console.WriteLine(View.INSTRUCT_3);
            Console.WriteLine(View.INSTRUCT_4);
            Console.WriteLine(View.INSTRUCT_5);
        }
    }
}
