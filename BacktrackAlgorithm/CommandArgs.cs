﻿using System;
namespace BacktrackAlgorithm
{
    public class CommandArguements
    {
        public enum enumCommands
        {
            Unknown = 0,
            Help = 1,
            Exit = 2,
            Clear = 3,
            Print = 4,
            Add = 5,
            Find = 6,
            Break = 7
        }

        public enum enumCommandModifiers
        {
            Default = 0,
            Shortest = 1,
            Longest = 2,
            Preserve = 3
        }

        public CommandArguements(string userInput)
        {
            var commandParts = userInput.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            if (commandParts.Length > 0)
            {
                _SetCommand(commandParts[0]);
            }
            if (commandParts.Length > 1)
            {
                FirstNode = commandParts[1];
            }
            else
            {
                FirstNode = string.Empty;
            }
            if (commandParts.Length > 2)
            {
                SecondNode = commandParts[2];
            }
            else
            {
                SecondNode = string.Empty;
            }
            if (commandParts.Length > 3)
            {
                _SetCommandModifier(commandParts[3]);
            }
            if((Command == enumCommands.Add || Command == enumCommands.Break || Command == enumCommands.Find)
                && (FirstNode == string.Empty || SecondNode == string.Empty))
            {
                _SetCommand("unknown");
            }
        }

        public string FirstNode { get; set; }
        public string SecondNode { get; set; }

        private enumCommands _command;
        public enumCommands Command {
            get
            {
                return _command;
            }
        }

        private void _SetCommand(string input)
        {
            switch (input.ToLower())
            {
                case "help":
                    _command = enumCommands.Help;
                    break;
                case "exit":
                    _command = enumCommands.Exit;
                    break;
                case "clear":
                    _command = enumCommands.Clear;
                    break;
                case "print":
                    _command = enumCommands.Print;
                    break;
                case "add":
                    _command = enumCommands.Add;
                    break;
                case "break":
                    _command = enumCommands.Break;
                    break;
                case "find":
                    _command = enumCommands.Find;
                    break;
                default:
                    _command = enumCommands.Unknown;
                    break;
            }
        }

        private enumCommandModifiers _commandModifier;
        public enumCommandModifiers CommandModifier
        {
            get
            {
                return _commandModifier;
            }
        }

        private void _SetCommandModifier(string input)
        {
            switch (input.ToLower())
            {
                case "s":
                case "-s":
                    _commandModifier = enumCommandModifiers.Shortest;
                    break;
                case "l":
                case "-l":
                    _commandModifier = enumCommandModifiers.Longest;
                    break;
                case "p":
                case "-p":
                    _commandModifier = enumCommandModifiers.Preserve;
                    break;
                default:
                    _commandModifier = enumCommandModifiers.Default;
                    break;
            }
        }
    }
}
