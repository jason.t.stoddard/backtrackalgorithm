﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BacktrackAlgorithm
{
    class Program
    {
        static Dictionary<string, List<string>> _graph;

        static void Main(string[] args)
        {
            _Init();

            while (true)
            {
                var userInput = Console.ReadLine();

                CommandArguements userCommand = _GetUserCommandArguement(userInput);

                if (userCommand.Command == CommandArguements.enumCommands.Exit) { break; }

                var commandResult = _ProcessUserInput(userCommand);

                View.PrintCommandResult(commandResult);

                View.PrintBlank();

                View.PrintDefaultPrompt();             
            } 

            Environment.Exit(0);
        }

        private static void _Init()
        {
            _graph = new Dictionary<string, List<string>>();
            View.PrintWelcome();
            View.PrintBlank();
            View.PrintDefaultPrompt();
        }

        private static CommandArguements _GetUserCommandArguement(string userInput)
        {
            return new CommandArguements(userInput);
        }

        private static string _ProcessUserInput(CommandArguements userCommands)
        {
            switch (userCommands.Command)
            {
                case CommandArguements.enumCommands.Help:
                    return _HandleHelp();

                case CommandArguements.enumCommands.Clear:
                    return _HandleClear();

                case CommandArguements.enumCommands.Print:
                    return _HandlePrintNode(userCommands.FirstNode);

                case CommandArguements.enumCommands.Add:
                    return _HandleAddNode(userCommands.FirstNode, userCommands.SecondNode);

                case CommandArguements.enumCommands.Find:
                    return _HandleFindNode(userCommands.FirstNode, userCommands.SecondNode, userCommands.CommandModifier);

                case CommandArguements.enumCommands.Break:
                    return _HandleDisconnectNode(userCommands.FirstNode, userCommands.SecondNode, userCommands.CommandModifier);

                default:
                    return "Error: Unhandled action!";
            }
        }

        private static string _HandleHelp()
        {
            View.PrintHelpMessage();
            return string.Empty;
        }

        private static string _HandleClear()
        {
            _graph.Clear();
            Console.Clear();
            return "graph cleared";
        }

        private static string _HandlePrintNode(string nodeName)
        {
            if (_graph.ContainsKey(nodeName))
            {
                return $"({_graph[nodeName].Count} connected node{(_graph[nodeName].Count == 1 ? "" : "s")}) " +
                    $"{string.Join(",", _graph[nodeName].ToArray())}";
            }
            else
            {
                return "node not found";
            }
        }

        private static string _HandleAddNode(string firstNode, string secondNode)
        {
            _AddToDictionary(firstNode, secondNode);
            _AddToDictionary(secondNode, firstNode);
            return "add successful";
        }

        private static string _HandleDisconnectNode(string firstNode, string secondNode, CommandArguements.enumCommandModifiers actionModifier)
        {
            bool isPreserveOrphan = false;
            if (actionModifier == CommandArguements.enumCommandModifiers.Preserve) { isPreserveOrphan = true; }
            _RemoveFromDictionary(firstNode, secondNode, isPreserveOrphan);
            _RemoveFromDictionary(secondNode, firstNode, isPreserveOrphan);
            return "break successful";
        }

        private static void _AddToDictionary(string keyNode, string valueNode)
        {
            if (!_graph.ContainsKey(keyNode))
            {
                _graph.Add(keyNode, new List<string>());
            }

            if (!_graph[keyNode].Contains(valueNode))
            {
                _graph[keyNode].Add(valueNode);
            }
        }

        private static void _RemoveFromDictionary(string keyNode, string valueNode, bool isPreserveOrphan)
        {
            if (_graph.ContainsKey(keyNode) && _graph[keyNode].Contains(valueNode))
            {
                _graph[keyNode].Remove(valueNode);
                if (! isPreserveOrphan && _graph[keyNode].Count == 0)
                {
                    _graph.Remove(keyNode);
                }
            }
        }

        private static string _HandleFindNode(string startNode, string targetNode, CommandArguements.enumCommandModifiers actionModifier)
        {
            if (_graph.ContainsKey(startNode) && _graph.ContainsKey(targetNode))
            {
                
                var startTree = new List<Stack<string>> { new Stack<string>(new[] { startNode }) };

                var searchedNodes = new List<string>();

                var successTrees = new List<Stack<string>>();

                _SeekNode(targetNode, searchedNodes, startTree, ref successTrees);

                var printTree = string.Empty;

                if (successTrees.Count > 0 && successTrees[0].Count > 0)
                {
                    Stack<string> getTree = null;
                    switch (actionModifier)
                    {
                        case CommandArguements.enumCommandModifiers.Shortest:
                            getTree = successTrees.First(y => y.Count == successTrees.Min(x => x.Count));
                            break;
                        case CommandArguements.enumCommandModifiers.Longest:
                            getTree = successTrees.First(y => y.Count == successTrees.Max(x => x.Count));
                            break;
                        default:
                            getTree = successTrees[0];
                            break;
                    }

                    var reorderTree = _ReverseStack(getTree);
                    printTree = $"({reorderTree.Count} nodes) {string.Join(", ", reorderTree.ToArray())}";
                    
                }
                else
                {
                    printTree = $"node {startNode} is not related to {targetNode}";
                }

                return printTree;
 
            }
            else
            {
                return "1 or both nodes are not part of the graph";
            }
                
        }
        
        
        private static void _SeekNode(string targetNode, List<string> searchedNodes,
            List<Stack<string>> inProcessTrees, ref List<Stack<string>> successTrees)
        {
            var inProcessTemp = new List<Stack<string>>();

            foreach(Stack<string> s in inProcessTrees)
            {
                var nodekey = s.Peek();
                if (_graph.ContainsKey(nodekey))
                {
                    foreach (string node in _graph[nodekey])
                    {
                        if (!searchedNodes.Contains(node))
                        {
                            Stack<string> sTemp = _CopyStack(s);
                          
                            sTemp.Push(node);
                        
                            if (sTemp.Peek() == targetNode)
                            {
                                successTrees.Add(sTemp);
                            }
                            else
                            {
                                inProcessTemp.Add(sTemp);
                            }
                        }
                    }
                    searchedNodes.Add(nodekey);
                }
            }

            if (inProcessTemp.Count > 0)
            {
                _SeekNode(targetNode, searchedNodes, inProcessTemp,ref successTrees);
            }
            
        }

        private static Stack<string> _CopyStack(Stack<string> stack)
        {
            var tempStack = new Stack<string>(stack);
            return new Stack<string>(tempStack);
        }

        private static Stack<string> _ReverseStack(Stack<string> stack)
        {
            var temp = new Stack<string>();

            if(stack != null) {
                while (stack.Count != 0)
                {
                    temp.Push(stack.Pop());
                }
            }
            
            return temp;
        }

    }
}
